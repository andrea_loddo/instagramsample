package sample.instagram.single_picture.presenter;

import com.android.volley.VolleyError;

import sample.instagram.common.NetworkPresenter;

/**
 * Created by Andrea on 08.03.2016.
 * Interface used for the callbacks from the model
 */
public interface SinglePictureActivityPresenterProvidedOps extends NetworkPresenter{
    void publishSuccessfulLikeInViewCallback(boolean isLiked);
    void publishErrorLikeInViewCallback(VolleyError error);
}
