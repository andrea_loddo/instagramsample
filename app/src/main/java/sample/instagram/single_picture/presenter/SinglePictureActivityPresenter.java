package sample.instagram.single_picture.presenter;

import android.content.Context;

import com.android.volley.VolleyError;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import sample.instagram.main_activity.model.response.MediaResponse;
import sample.instagram.single_picture.model.InstagramLikeRequestVolley;
import sample.instagram.single_picture.model.InstagramLikeRequestRetrofit;
import sample.instagram.single_picture.view.SinglePictureActivityView;

/**
 * Created by Andrea on 07/03/2016.
 */
public class SinglePictureActivityPresenter
        extends MvpBasePresenter<SinglePictureActivityView>
        implements SinglePictureActivityPresenterProvidedOps{

    Context context;

    public SinglePictureActivityPresenter(Context context) {
        this.context = context;
    }

    public void favouriteClicked(String id, boolean isLiked, boolean requestWithRetrofit){
        if(isLiked){
            if (requestWithRetrofit) requestUnlikeWithRetrofitAndRx(id);
            else requestUnlikeWithVolley(id);
        }
        else{
            if (requestWithRetrofit) requestLikeWithRetrofitAndRx(id);
            else requestLikeWithVolley(id);
        }
    }

    /**
     * POST request for the InstagramLikeRequestVolley
     * @param id The picture id
     */
    private void requestLikeWithVolley(String id){
        String url = id+ "/likes?access_token=" + TOKEN;
        InstagramLikeRequestVolley.startRequest(url, context, this, false);
    }

    /**
     * DELETE request for the InstagramLikeRequestVolley
     * @param id The picture id
     */
    private void requestUnlikeWithVolley(String id){
        String url = id+ "/likes";
        InstagramLikeRequestVolley.startRequest(url, context, this, true);
    }

    /**
     * Builds a Retrofit instance and returns the service
     * @return
     */
    private InstagramLikeRequestRetrofit buildRetrofitService(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        return retrofit.create(InstagramLikeRequestRetrofit.class);
    }

    /**
     * Call the POST method for LIKE endpoint
     * Add RX Subscriber for managing the result
     * @param id The picture id
     */
    private void requestLikeWithRetrofitAndRx(String id){

        InstagramLikeRequestRetrofit service = buildRetrofitService();
        service.postLike(id, TOKEN)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSubscriber(true));;
    }

    /**
     * Call the DELETE method for LIKE endpoint.
     * Add RX Subscriber for managing the result
     * @param id The picture id
     */
    private void requestUnlikeWithRetrofitAndRx(String id){

        InstagramLikeRequestRetrofit service = buildRetrofitService();
        service.deleteLike(id, TOKEN)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSubscriber(false));
    }

    /**
     * Creates a RX Subscriber that forwards the result to the View.
     * @param isLike Indicates whether the like icon should be shown or not.
     * @return The Subscriber that communicates with the View.
     */
    private Subscriber<MediaResponse> getSubscriber(boolean isLike){
        return new Subscriber<MediaResponse>() {
            @Override
            public void onCompleted() {
                if(isViewAttached()) getView().updateFavorite(isLike);
            }

            @Override
            public void onError(Throwable e) {
                if(isViewAttached()) getView().showErrorToast();
            }

            @Override
            public void onNext(MediaResponse mediaResponse) {
            }
        };
    }

    /**
     * If the call was successful inform the view
     * @param isLiked
     */
    @Override
    public void publishSuccessfulLikeInViewCallback(boolean isLiked) {
        if(isViewAttached()) getView().updateFavorite(isLiked);
    }

    /**
     * When an error in the network call occurred inform the view
     * @param error
     */
    @Override
    public void publishErrorLikeInViewCallback(VolleyError error) {
        if(isViewAttached()) getView().showErrorToast();
    }
}
