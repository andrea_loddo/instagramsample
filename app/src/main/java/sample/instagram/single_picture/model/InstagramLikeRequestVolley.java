package sample.instagram.single_picture.model;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import sample.instagram.common.VolleySingletonRequestQueue;
import sample.instagram.single_picture.presenter.SinglePictureActivityPresenterProvidedOps;

/**
 * Created by Andrea on 08.03.2016.
 */
public class InstagramLikeRequestVolley {
    /**
     * Static method with a clear name that calls the private constructor
     * @param url the complete url for the request
     * @param context context needed for the volley request
     * @param mainActivityPresenter interface needed to deliver the results to the caller
     * @param like indicates if the request wants to submit a like (true) or delete (false)
     */
    public static void startRequest(String url, Context context, SinglePictureActivityPresenterProvidedOps mainActivityPresenter, boolean like){
        new InstagramLikeRequestVolley(url, context, mainActivityPresenter, like);
    }

    private InstagramLikeRequestVolley(String url, Context context, SinglePictureActivityPresenterProvidedOps singlePictureActivity, boolean like) {

        if(like){
            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(
                    Request.Method.POST,
                    singlePictureActivity.baseUrl+url,
                    new ResponseLikeListener(singlePictureActivity),
                    new ErrorListener(singlePictureActivity)){

                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<>();
                    params.put("access_token",singlePictureActivity.TOKEN);
                    return params;
                }
            };

            // Add the request to the RequestQueue.
            VolleySingletonRequestQueue.getInstance(context.getApplicationContext()).addToRequestQueue(stringRequest);
        }
        else{
            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(
                    Request.Method.DELETE,
                    singlePictureActivity.baseUrl+url,
                    new ResponseUnlikeListener(singlePictureActivity),
                    new ErrorListener(singlePictureActivity));

            // Add the request to the RequestQueue.
            VolleySingletonRequestQueue.getInstance(context.getApplicationContext()).addToRequestQueue(stringRequest);
        }
    }

    /**
     * Listener passed to the request, called when a request ended with an error
     */
    private static class ErrorListener implements Response.ErrorListener {

        private final SinglePictureActivityPresenterProvidedOps singlePictureActivity;

        public ErrorListener(SinglePictureActivityPresenterProvidedOps singlePictureActivity) {
            this.singlePictureActivity = singlePictureActivity;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            singlePictureActivity.publishErrorLikeInViewCallback(error);
        }
    }


    /**
     * Listener passed to the request, called when a request was successful
     */
    private class ResponseLikeListener implements Response.Listener<String> {
        private final SinglePictureActivityPresenterProvidedOps singlePictureActivity;

        public ResponseLikeListener(SinglePictureActivityPresenterProvidedOps singlePictureActivity) {
            this.singlePictureActivity = singlePictureActivity;
        }

        @Override
        public void onResponse(String response) {
            singlePictureActivity.publishSuccessfulLikeInViewCallback(true);
        }
    }

    /**
     * Listener passed to the request, called when a request was successful
     */
    private class ResponseUnlikeListener implements Response.Listener<String> {
        private final SinglePictureActivityPresenterProvidedOps singlePictureActivity;

        public ResponseUnlikeListener(SinglePictureActivityPresenterProvidedOps singlePictureActivity) {
            this.singlePictureActivity = singlePictureActivity;
        }

        @Override
        public void onResponse(String response) {
            singlePictureActivity.publishSuccessfulLikeInViewCallback(false);
        }
    }

}
