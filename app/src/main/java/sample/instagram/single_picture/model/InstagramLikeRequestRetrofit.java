package sample.instagram.single_picture.model;

import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;
import sample.instagram.main_activity.model.response.MediaResponse;

/**
 * Created by Andrea on 07.04.2016.
 */
public interface InstagramLikeRequestRetrofit {

    @POST("{media-id}/likes?")
    Observable<MediaResponse> postLike(@Path("media-id") String id, @Query("access_token") String access_token);

    @DELETE("{media-id}/likes")
    Observable<MediaResponse> deleteLike(@Path("media-id") String id, @Query("access_token") String access_token);
}
