package sample.instagram.single_picture.view;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import sample.instagram.main_activity.model.response.InstagramResponse;

/**
 * Created by Andrea on 07/03/2016.
 * Used to define the contracts methods that the presenter can call.
 */

public interface SinglePictureActivityView extends MvpLceView<InstagramResponse> {
    void updateFavorite(boolean isLiked);
    void showErrorToast();
}
