package sample.instagram.single_picture.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hannesdorfmann.mosby.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.MvpLceViewStateActivity;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.data.RetainingLceViewState;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sample.instagram.R;
import sample.instagram.main_activity.model.response.InstagramResponse;
import sample.instagram.main_activity.view.MainActivity;
import sample.instagram.single_picture.presenter.SinglePictureActivityPresenter;
import sample.instagram.common.MyOnGlobalLayoutListener;
import sample.instagram.main_activity.view.MainActivityView;

/**
 * Created by Andrea on 07/03/2016.
 *
 * This activity shows a full screen picture. The user can like the picture or get directions.
 */
public class SinglePictureActivity
        extends MvpLceViewStateActivity<RelativeLayout, InstagramResponse, SinglePictureActivityView, SinglePictureActivityPresenter>
        implements SinglePictureActivityView{

    @Bind(R.id.imageViewSingleActivity) ImageView imageViewSingleActivity;
    @Bind(R.id.favouriteLayout) ViewGroup favouriteLayout;
    @Bind(R.id.navigationLayout) ViewGroup navigationLayout;
    @Bind(R.id.shortcutsLayout) ViewGroup shortcutsLayout;
    @Bind(R.id.toolbar) ViewGroup toolbar;
    @Bind(R.id.textViewCaptionSinglePicture) TextView textViewCaptionSinglePicture;
    @Bind(R.id.textViewLocationSinglePicture) TextView textViewLocationSinglePicture;

    InstagramResponse mInstagramResponse;
    private boolean mItemHasChanged = false;

    /**
     * Setup toolbar
     * Binds all the views
     * Gets the intent from the previous activity and extract the InstagramResponse
     * Fill the views with text from the InstagramResponse
     * and finally check if the picture was liked
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_picture_activity);
        setRetainInstance(true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        mInstagramResponse = (InstagramResponse) intent.getSerializableExtra(MainActivityView.ITEM);
        textViewCaptionSinglePicture.setText(mInstagramResponse.getCaption().getText());
        textViewLocationSinglePicture.setText(mInstagramResponse.getLocation().getName());
        imageViewSingleActivity.getViewTreeObserver().addOnGlobalLayoutListener(new MyOnGlobalLayoutListener(this, imageViewSingleActivity, mInstagramResponse));

        updateFavoriteButton(mInstagramResponse.getUserHasLiked());
    }

    /**
     * Manage the click on the back arrow
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            stopActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Manage the click on the physical back button
     */
    @Override public void onBackPressed(){
        stopActivity();
    }

    /**
     * Inform the previous activity that something has changed and that it has to load again the pictures
     */
    private void stopActivity(){
        setResult(mItemHasChanged ?RESULT_OK:RESULT_CANCELED);
        finish();
    }

    @NonNull
    @Override
    public SinglePictureActivityPresenter createPresenter() {
        return new SinglePictureActivityPresenter(this);
    }

    /**
     * Butterknife annotation, no need to manually attach a OnClickListener
     * Hide the toolbar
     */
    @OnClick(R.id.imageViewSingleActivity)
    public void imageClicked(){
        toggleToolbarLayout(shortcutsLayout);
        toggleToolbarLayout(toolbar);
    }

    private void toggleToolbarLayout(ViewGroup viewGroup){
        if(viewGroup.getVisibility() == View.VISIBLE) viewGroup.setVisibility(View.GONE);
        else viewGroup.setVisibility(View.VISIBLE);
    }

    /**
     * Butterknife annotation, no need to manually attach a OnClickListener
     * Starts an intent for google maps
     */
    @OnClick(R.id.navigationLayout)
    public void startGoogleMaps(){
        String coordinates = "http://maps.google.com/maps?daddr=" + mInstagramResponse.getLocation().getLatitude() + "," + mInstagramResponse.getLocation().getLongitude();
        Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse(coordinates) );
        startActivity( intent );
    }

    /**
     * Butterknife annotation, no need to manually attach a OnClickListener
     * Forwards the click event to the presenter and the current status used to determine if a Like or Unlike request should be started
     */
    @OnClick(R.id.favouriteLayout)
    public void favouriteButtonClicked(){
        presenter.favouriteClicked(mInstagramResponse.getId(), mInstagramResponse.getUserHasLiked(), MainActivity.shouldUseRetrofitAndRx);
    }


    /**
     * Called back from presenter when the Like/Unlike request was done
     * @param isLiked indicates the action to be done, is forwarded to updateFavoriteButton(boolean isLiked)
     */
    @Override
    public void updateFavorite(boolean isLiked) {
        mItemHasChanged = true;
        mInstagramResponse.setUserHasLiked(isLiked);
        updateFavoriteButton(isLiked);
    }

    /**
     * Updates the icon in the layout
     * @param isLiked
     */
    private void updateFavoriteButton(boolean isLiked){
        if(isLiked){
            favouriteLayout.setBackground(getDrawable(R.drawable.ic_favorite_red_300_48dp));
        }
        else{
            favouriteLayout.setBackground(getDrawable(R.drawable.ic_favorite_border_grey_100_48dp));
        }
    }

    /**
     * Informs the user that his her action was not successful
     */
    @Override
    public void showErrorToast() {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(this, R.string.singlepicture_error, duration);
        toast.show();
    }

    @Override
    public LceViewState<InstagramResponse, SinglePictureActivityView> createViewState() {
        return new RetainingLceViewState<>();
    }

    @Override
    public InstagramResponse getData() {
        return mInstagramResponse;
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Override
    public void setData(InstagramResponse data) {
        mInstagramResponse = data;
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        showContent();
    }
}
