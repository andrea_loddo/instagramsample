package sample.instagram.main_activity.presenter;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import sample.instagram.common.NetworkPresenter;
import sample.instagram.main_activity.model.response.InstagramResponse;

/**
 * Created by Andrea on 08.03.2016.
 */
public interface MainActivityPresenterProvidedOps extends NetworkPresenter{
    void publishSuccessInViewCallback(ArrayList<InstagramResponse> instagramResponses);
    void publishErrorInViewCallback(VolleyError error);
}
