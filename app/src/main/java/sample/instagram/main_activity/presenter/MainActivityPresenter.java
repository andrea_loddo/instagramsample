package sample.instagram.main_activity.presenter;

import android.content.Context;

import com.android.volley.VolleyError;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.io.ObjectStreamException;
import java.util.ArrayList;
import java.util.Observable;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import sample.instagram.common.VolleySingletonRequestQueue;
import sample.instagram.main_activity.model.InstagramMediaRequestRetrofit;
import sample.instagram.main_activity.model.InstagramMediaRequestVolley;
import sample.instagram.main_activity.model.response.InstagramResponse;
import sample.instagram.main_activity.model.response.MediaResponse;
import sample.instagram.main_activity.view.MainActivityView;

/**
 * Created by Andrea on 07/03/2016.
 */
public class MainActivityPresenter
        extends MvpBasePresenter<MainActivityView>
        implements MainActivityPresenterProvidedOps, Observer<MediaResponse>{

    private Context context;

    public MainActivityPresenter(Context context) {
        this.context = context;
    }

    public void load(boolean requestWithRetrofit) {
        if (requestWithRetrofit) requestWithRetrofitAndRx();
        else requestWithVolley();
    }

    private void requestWithVolley(){
        String url ="search?lat=" + latitude + "&lng=" + longitude + "&access_token="+ TOKEN + "&distance=" + distance;
        InstagramMediaRequestVolley.startRequest(url, context, this);
    }

    private void requestWithRetrofitAndRx(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.instagram.com/v1/media/")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        InstagramMediaRequestRetrofit service = retrofit.create(InstagramMediaRequestRetrofit.class);

        service.getUser(latitude, longitude, TOKEN, distance)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this);
    }

    /*
    Callback methods for the volley requests.
     */
    @Override
    public void publishSuccessInViewCallback(ArrayList<InstagramResponse> instagramResponses){
        if(isViewAttached()){
            getView().setData(instagramResponses);
            getView().showContent();
        }
    }

    @Override
    public void publishErrorInViewCallback(VolleyError error) {
        if(isViewAttached()) getView().showError(error, false);
    }

    /*
    Implementation of the Observer interface.
    Called when Rx + Retrofit is used
     */
    @Override
    public void onCompleted() {
        if(isViewAttached()) getView().showContent();
    }

    @Override
    public void onError(Throwable e) {
        if(isViewAttached()) getView().showError(e, false);
    }

    @Override
    public void onNext(MediaResponse mediaResponse) {
        if(isViewAttached()) getView().setData((ArrayList) mediaResponse.getData());
    }
}
