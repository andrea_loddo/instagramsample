package sample.instagram.main_activity.model;

import java.util.ArrayList;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;
import sample.instagram.main_activity.model.response.InstagramResponse;
import sample.instagram.main_activity.model.response.MediaResponse;

/**
 * Created by Andrea on 06.04.2016.
 */
public interface InstagramMediaRequestRetrofit {

    @GET("search?")
    Observable<MediaResponse> getUser(
            @Query("lat") String latitude,
            @Query("lng") String longitude,
            @Query("access_token") String access_token,
            @Query("distance") String distance);
}
