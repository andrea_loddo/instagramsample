package sample.instagram.main_activity.model;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import sample.instagram.common.VolleySingletonRequestQueue;
import sample.instagram.main_activity.model.response.InstagramResponse;
import sample.instagram.main_activity.presenter.MainActivityPresenterProvidedOps;

/**
 * Created by Andrea on 08.03.2016.
 */
public class InstagramMediaRequestVolley {

    /**
     * Static method with a clear name that calls the private constructor
     * @param url the complete url for the request
     * @param context context needed for the volley request
     * @param mainActivityPresenter interface needed to deliver the results to the caller
     */
    public static void startRequest(String url, Context context, MainActivityPresenterProvidedOps mainActivityPresenter){
        new InstagramMediaRequestVolley(url, context, mainActivityPresenter);
    }

    private InstagramMediaRequestVolley(String url, Context context, final MainActivityPresenterProvidedOps mainActivityPresenter) {

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                mainActivityPresenter.baseUrl + url,
                new ResponseListener(mainActivityPresenter),
                new ErrorListener(mainActivityPresenter));

        VolleySingletonRequestQueue.getInstance(context.getApplicationContext()).addToRequestQueue(stringRequest);
    }

    /**
     * Listener passed to the request, called when a request ended with an error
     */
    private static class ErrorListener implements Response.ErrorListener {

        private final MainActivityPresenterProvidedOps mainActivityPresenter;

        public ErrorListener(MainActivityPresenterProvidedOps mainActivityPresenter) {
            this.mainActivityPresenter = mainActivityPresenter;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            mainActivityPresenter.publishErrorInViewCallback(error);
        }
    }


    /**
     * Listener passed to the request, called when a request was successful
     */
    private class ResponseListener implements Response.Listener<String> {
        private final MainActivityPresenterProvidedOps mainActivityPresenter;

        public ResponseListener(MainActivityPresenterProvidedOps mainActivityPresenter) {
            this.mainActivityPresenter = mainActivityPresenter;
        }

        @Override
        public void onResponse(String response) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                JSONObject object = new JSONObject(response);

                JSONArray jsonArray = (JSONArray) object.get("data");

                ArrayList<InstagramResponse> instagramResponseList = new ArrayList<>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject temp = jsonArray.getJSONObject(i);
                    InstagramResponse instagramResponse = mapper.readValue(temp.toString(), InstagramResponse.class);

                    //check that the item is an image, not ready for videos
                    if(instagramResponse.getType().equals("image") )
                        instagramResponseList.add(instagramResponse);
                }

                //callback to the presenter with the results
                mainActivityPresenter.publishSuccessInViewCallback(instagramResponseList);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
