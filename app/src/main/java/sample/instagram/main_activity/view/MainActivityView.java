package sample.instagram.main_activity.view;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import java.util.ArrayList;

import sample.instagram.main_activity.model.response.InstagramResponse;

/**
 * Created by Andrea on 07/03/2016.
 * Normally used to define the contracts methods that the presenter can call.
 * In this case there aren't any, in any case the interface is needed
 */
public interface MainActivityView extends MvpLceView<ArrayList<InstagramResponse>> {
    String ITEM = "THE ITEM BEING SENT";
}
