package sample.instagram.main_activity.view;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hannesdorfmann.annotatedadapter.annotation.Field;
import com.hannesdorfmann.annotatedadapter.annotation.ViewField;
import com.hannesdorfmann.annotatedadapter.annotation.ViewType;
import com.hannesdorfmann.annotatedadapter.support.recyclerview.SupportAnnotatedAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import sample.instagram.R;
import sample.instagram.main_activity.model.response.InstagramResponse;
import sample.instagram.single_picture.view.SinglePictureActivity;

/**
 * Created by Andrea on 07/03/2016.
 *
 * Adapter for the images on MainActivity
 */
public class ThumbnailPictureAdapter extends SupportAnnotatedAdapter implements ThumbnailPictureAdapterBinder {

    //Context needed in order to download pictures and start new activity
    Activity mActivity;
    int mExactElementSideMeasure = -1;

    //Keep an ArrayList with all the items
    ArrayList<InstagramResponse> instagramResponses;

    public ThumbnailPictureAdapter(Activity activity, int elementWidth) {
        super(activity);
        this.mActivity = activity;
        this.mExactElementSideMeasure = elementWidth; //images have a square form
        instagramResponses = new ArrayList<>();
    }

    @ViewType(
            layout = R.layout.home_item,   // The layout that will be inflated for this view type
            views = {
                    @ViewField(id = R.id.imageView,name = "imageView",type = ImageView.class),
                    @ViewField(id = R.id.captionText,name = "captionText",type = TextView.class)
            },
            fields = {
                    @Field(type = DetailsClickListener.class, name = "detailsClickListener" )
            }
    ) public final int NORMAL_ELEMENT = 0;

    @Override
    public int getItemCount() {
        return instagramResponses.size();
    }

    /**
     * Called when items are downloaded and are added to adapter.
     * @param items
     */
    protected void addItems(ArrayList<InstagramResponse> items){
        try{
            instagramResponses.clear();
            instagramResponses.addAll(items);
        }catch (Exception e){
            e.printStackTrace();

            instagramResponses = new ArrayList<>();
            instagramResponses.addAll(items);
        }
    }

    /**
     * Must be able to return all items for the ViewState to work
     * @return
     */
    protected ArrayList<InstagramResponse> getItems(){
        return instagramResponses;
    }

    /**
     *
     * This methods sets the height of the elements in the grid, then loads the pictures.
     * Then fills the viewholder with information contained in PictureItem.
     * Must always check if there is a caption, could be null.
     * Finally add a clickListener for starting the next activity
     * @param vh
     * @param position
     */
    @Override
    public void bindViewHolder(ThumbnailPictureAdapterHolders.NORMAL_ELEMENTViewHolder vh, int position) {
        final InstagramResponse item  = instagramResponses.get(position);

        vh.imageView.getLayoutParams().height = mExactElementSideMeasure;
        
        Picasso.with(mActivity).
                load(item.getImages().getLowResolution().getUrl()).
                resize(mExactElementSideMeasure, mExactElementSideMeasure).
                into(vh.imageView);

        if(item.getCaption()!=null)
            vh.captionText.setText(item.getCaption().getText());
        vh.detailsClickListener = new DetailsClickListener(item,mActivity);
        vh.itemView.setOnClickListener(vh.detailsClickListener);
    }

    /**
     * Listens for clicks on elements, and if clicked start new FullScreen activity
     */
    class DetailsClickListener implements View.OnClickListener{
        InstagramResponse instagramResponse;
        Activity mActivity;

        public DetailsClickListener(InstagramResponse instagramResponse,  Activity activity) {
            this.instagramResponse = instagramResponse;
            this.mActivity = activity;
        }

        /**
         * Create an intent and adds the item that is going to be shown fulll screen.
         * Then start the activity.
         * @param v
         */
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mActivity, SinglePictureActivity.class);
            intent.putExtra(MainActivityView.ITEM, instagramResponse);
            mActivity.startActivityForResult(intent, MainActivity.ACTIVITY_CODE);
        }
    }

}
