package sample.instagram.main_activity.view;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;

import com.hannesdorfmann.mosby.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.MvpLceViewStateActivity;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.data.RetainingLceViewState;
import java.util.ArrayList;
import butterknife.Bind;
import butterknife.ButterKnife;
import sample.instagram.R;
import sample.instagram.main_activity.model.response.InstagramResponse;
import sample.instagram.main_activity.presenter.MainActivityPresenter;

public class MainActivity
        extends MvpLceViewStateActivity<SwipeRefreshLayout, ArrayList<InstagramResponse>, MainActivityView, MainActivityPresenter>
        implements MainActivityView,SwipeRefreshLayout.OnRefreshListener{

    static final int ACTIVITY_CODE = 1000;
    static final int PREFERRED_ELEMENT_WIDTH_IN_DP = 200;
    public static final boolean shouldUseRetrofitAndRx = true;

    int mExactElementWidth = -1;
    int mNumberOfColumns = 2;

    private ThumbnailPictureAdapter thumbnailPictureAdapter;
    private GridLayoutManager mGridLayoutManager;
    @Bind(R.id.recyclerListView) RecyclerView recyclerListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRetainInstance(true); // Enable retaining viewstate

        //Setup the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Bind UI elements with Butterknife
        ButterKnife.bind(this);


        mGridLayoutManager = new GridLayoutManager(this,mNumberOfColumns);
        setUpLayout();
        recyclerListView.setLayoutManager(mGridLayoutManager);

        //Initialize adapter for pictures and set it on the Recycler View
        thumbnailPictureAdapter = new ThumbnailPictureAdapter(this, mExactElementWidth);
        recyclerListView.setAdapter(thumbnailPictureAdapter);

        //Setup refresh listener for swipe down/update action
        contentView.setOnRefreshListener(this);
        contentView.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary);
    }

    /**
     * Determines and sets the number of columns shown
     */
    private void setUpLayout(){
        determineNumberAndWidthOfColumns();
        mGridLayoutManager.setSpanCount(mNumberOfColumns);
    }

    /**
     * Determines the number of columns shown.
     * Get the width and divide it by the PREFERRED_ELEMENT_WIDTH_IN_DP
     */
    private void determineNumberAndWidthOfColumns(){
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;

        mNumberOfColumns = Math.round(dpWidth/PREFERRED_ELEMENT_WIDTH_IN_DP);
        if(mNumberOfColumns%2 != 0) mNumberOfColumns++;//show only even number of images

        mExactElementWidth = Math.round(displayMetrics.widthPixels/mNumberOfColumns);
    }

    /**
     * Redraw the grid with pictures because with orientation change also the width changes
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setUpLayout();
    }

    @NonNull
    @Override
    public MainActivityPresenter createPresenter() {
        return new MainActivityPresenter(this);
    }

    @Override
    public LceViewState<ArrayList<InstagramResponse>, MainActivityView> createViewState() {
        return new RetainingLceViewState<>();
    }

    /**
     * Pass the loaded elements so that we don't have to load them again if the activity is paused or its orientation is changed
     * @return
     */
    @Override
    public ArrayList<InstagramResponse> getData() {
        return thumbnailPictureAdapter!=null?thumbnailPictureAdapter.getItems():null;
    }

    /**
     *
     * @param e
     * @param pullToRefresh
     * @return The error String that is going to be shown if there is a networking error
     */
    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        contentView.setRefreshing(false);
        String errorString = getString(R.string.mainactivity_error_network);
        return errorString;
    }

    /**
     * Adds the data received from the presenter to the adapter that will show the actual images
     * @param data
     */
    @Override
    public void setData(ArrayList<InstagramResponse> data) {
        thumbnailPictureAdapter.addItems(data);
        thumbnailPictureAdapter.notifyDataSetChanged();
    }

    /**
     * Called when data is set, hide the loading view
     */
    @Override public void showContent() {
        super.showContent();
        contentView.setRefreshing(false);
    }

    /**
     * Called when data is loading
     * @param pullToRefresh indicates whether it was a user refresh or was automatically called
     */
    @Override public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
        contentView.setRefreshing(pullToRefresh);
    }

    /**
     * It is called when we should load the data from network
     * @param pullToRefresh indicates whether it was a user refresh or was automatically called
     */
    @Override
    public void loadData(boolean pullToRefresh) {
        showLoading(pullToRefresh);
        presenter.load(shouldUseRetrofitAndRx);
    }

    /**
     * Called when user drags down and explicitely wants to update the view with new data
     */
    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ACTIVITY_CODE && resultCode == RESULT_OK)
            loadData(false);
    }
}
