package sample.instagram.common;

import android.content.Context;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import sample.instagram.main_activity.model.response.InstagramResponse;

/**
 * Created by Andrea on 08.03.2016.
 *
 * This class is used to listen when a layout is ready to be drawn, otherwise we cannot measure the
 * mImageView and the consequence is that we can't adapt the pictures measures
 */
public class MyOnGlobalLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
    private Context mContext;
    private final ImageView mImageView;
    private final InstagramResponse mInstagramResponse;

    public MyOnGlobalLayoutListener(Context context,
                                    ImageView imageView,
                                    InstagramResponse instagramResponse) {
        this.mContext = context;
        this.mImageView = imageView;
        this.mInstagramResponse = instagramResponse;
    }

    // Wait until layout to call Picasso
    @Override
    public void onGlobalLayout() {
        // Ensure we call this only once
        mImageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

        // see on the app an indicator of the origin of images: network, disk, memory
        //Picasso.with(mContext).setIndicatorsEnabled(true);

        Picasso.with(mContext)
                .load(mInstagramResponse.getImages().getStandardResolution().getUrl())
                .resize(mImageView.getWidth(), 0) //take the available width and adapt the height
                .into(mImageView);
    }
}
