package sample.instagram.common;

/**
 * Created by Andrea on 08.04.2016.
 */
public interface NetworkPresenter {
    String baseUrl = "https://api.instagram.com/v1/media/";
    String TOKEN = "3017160024.03bb033.5c87786d9cce46459a2b9c15794aadce";

    //Hardcoded parameters for the request
    String latitude = "52.4986";
    String longitude = "13.4185";
    String distance = "5000";
}
