# SAMPLE INSTAGRAM APPLICATION

Sample application (target: only Android M) that uses Instagram API to search for  pictures within a (hardcoded for now) radius. 

The app shows a grid with pictures (because Instagram API needs special permissions to access public content, I show here only pictures from my account, purposely uploaded within the radius) and allows full screen mode. The user can also open Google Maps to navigate to the place where the picture was taken and like the picture (using Instagram API).

I used the following open source libraries:

* Butterknife (for view binding): https://github.com/JakeWharton/butterknife
* Mosby (MVP architecture library): https://github.com/sockeqwe/mosby
* Picasso (library for pictures): https://github.com/square/picasso
* Jackson (JSON conversion to POJO): https://github.com/FasterXML/jackson
* Volley (HTTP library): https://android.googlesource.com/platform/frameworks/volley

Tested on:

* Samsung S3 device
* Emulated Nexus One (3,7") 
* Emulated tablet Nexus 10 (10"). 

Tool: Android Studio 2